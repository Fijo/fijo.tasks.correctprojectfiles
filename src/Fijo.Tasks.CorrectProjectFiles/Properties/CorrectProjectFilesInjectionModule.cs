﻿using Fijo.Infrastructure.DesignPattern.Observer.Impl;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using Fijo.Tasks.CorrectProjectFiles.Interface;
using Fijo.Tasks.CorrectProjectFiles.LoggingObserver;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Interface;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace Fijo.Tasks.CorrectProjectFiles.Properties {
	[PublicAPI]
	public class CorrectProjectFilesInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IFileContentReplaceService>().To<FileContentReplaceService>().InSingletonScope();
			kernel.Bind<ICorrectProjectFilesService>().To<CorrectProjectFilesService>().InSingletonScope();

			kernel.Bind<ILoggingObserver>().To<NantVerboseLoggingObserver>()
				.WhenInjectedInto<ObserverManager<ILoggingObserver, ILoggable>>()
				.InSingletonScope();
			kernel.Bind<IObserverManager<ILoggingObserver, ILoggable>>().To<ObserverManager<ILoggingObserver, ILoggable>>()
				.InScope(x => x.Request.Target.Type);
		}
	}
}
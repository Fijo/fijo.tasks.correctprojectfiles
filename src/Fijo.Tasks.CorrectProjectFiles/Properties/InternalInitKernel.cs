using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.Tasks.CorrectProjectFiles.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new CorrectProjectFilesInjectionModule());
		}
	}
}
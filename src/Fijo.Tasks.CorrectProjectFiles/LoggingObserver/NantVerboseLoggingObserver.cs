using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Interface;
using JetBrains.Annotations;
using NAnt.Core;

namespace Fijo.Tasks.CorrectProjectFiles.LoggingObserver {
	[PublicAPI]
	public class NantVerboseLoggingObserver : ILoggingObserver {
		private readonly Task _task;

		public NantVerboseLoggingObserver(Task task) {
			_task = task;
		}

		#region Implementation of IObserver
		public void Observe(IObserverable obj) {
			_task.Log(Level.Verbose, obj.ToString());
		}
		#endregion
	}
}
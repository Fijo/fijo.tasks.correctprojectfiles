using System.IO;
using System.Text.RegularExpressions;
using Fijo.Tasks.CorrectProjectFiles.Interface;
using JetBrains.Annotations;

namespace Fijo.Tasks.CorrectProjectFiles {
	[UsedImplicitly]
	public class FileContentReplaceService : IFileContentReplaceService {
		public void Replace(string file, Regex pattern, string replacement) {
			var content = File.ReadAllText(file);
			var replacedContent = pattern.Replace(content, replacement);
			File.WriteAllText(file, replacedContent);
		}
	}
}
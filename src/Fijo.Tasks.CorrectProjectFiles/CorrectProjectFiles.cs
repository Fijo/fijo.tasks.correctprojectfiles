using System;
using Fijo.Tasks.CorrectProjectFiles.Interface;
using Fijo.Tasks.CorrectProjectFiles.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using NAnt.Core;
using NAnt.Core.Attributes;

namespace Fijo.Tasks.CorrectProjectFiles {
	[PublicAPI]
	[TaskName("CorrectProjectFiles")]
	public class CorrectProjectFiles : Task {
		private readonly ICorrectProjectFilesService _correctProjectFilesService;

		public CorrectProjectFiles() {
			new InternalInitKernel().Init();
			Kernel.Inject.Bind<Task>().ToConstant(this);
			_correctProjectFilesService = Kernel.Resolve<ICorrectProjectFilesService>();
		}

		[TaskAttribute("Replacement", Required = true)]
		[PublicAPI]
		public string Replacement { get; set; }

		[TaskAttribute("Pattern", Required = true)]
		[PublicAPI]
		public string Pattern { get; set; }

		[TaskAttribute("FilesWithExtention", Required = true)]
		[PublicAPI]
		public string FilesWithExtention { get; set; }

		[TaskAttribute("Dictionary", Required = true)]
		[PublicAPI]
		public string Dictionary { get; set; }

		protected override void ExecuteTask() {
			Log(Level.Verbose, string.Format("Replacement: {0}", Replacement));
			Log(Level.Verbose, string.Format("Pattern: {0}", Pattern));
			Log(Level.Verbose, string.Format("FilesWithExtention: {0}", FilesWithExtention));
			Log(Level.Verbose, string.Format("Dictionary: {0}", Dictionary));

			_correctProjectFilesService.Correct(Dictionary, FilesWithExtention, Pattern, Replacement);
		}
	}
}

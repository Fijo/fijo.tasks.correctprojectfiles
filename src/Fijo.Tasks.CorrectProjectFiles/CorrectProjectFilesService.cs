using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using Fijo.Tasks.CorrectProjectFiles.Interface;
using FijoCore.Infrastructure.LightContrib.Module.FileTools;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Obj;
using FijoCore.Infrastructure.LightContrib.Repositories;
using JetBrains.Annotations;

namespace Fijo.Tasks.CorrectProjectFiles {
	[UsedImplicitly]
	public class CorrectProjectFilesService : ICorrectProjectFilesService {
		private readonly IObserverManager<ILoggingObserver, ILoggable> _observerManager;
		private readonly IFileContentReplaceService _fileContentReplacementService;
		private readonly IFileFilterService _fileFilterService;
		private readonly IAllFileRepository _allFileRepository;

		public CorrectProjectFilesService(IObserverManager<ILoggingObserver, ILoggable> observerManager, IFileContentReplaceService fileContentReplacementService, IFileFilterService fileFilterService, IAllFileRepository allFileRepository) {
			_observerManager = observerManager;
			_fileContentReplacementService = fileContentReplacementService;
			_fileFilterService = fileFilterService;
			_allFileRepository = allFileRepository;
		}

		public void Correct(string dictionary, string filesWithExtention, string pattern, string replacement) {
			#region PreCondition
			Debug.Assert(!filesWithExtention.Contains("."), @"!filesWithExtention.Contains(""."")");
			#endregion
			var regex = GetRegex(pattern);
			var affectedFiles = GetAffectedFiles(dictionary, filesWithExtention);
			foreach(var file in affectedFiles) HandleReplacementInFile(replacement, regex, file);
		}

		private void HandleReplacementInFile(string replacement, Regex regex, string file) {
			_observerManager.Notify(new LoggableString(string.Format("Replace File {0}", file)));
			_fileContentReplacementService.Replace(file, regex, replacement);
		}

		private IEnumerable<string> GetAffectedFiles(string dictionary, string filesWithExtention) {
			return _fileFilterService.FilterByExt(_allFileRepository.Get(dictionary), filesWithExtention);
		}

		private Regex GetRegex(string pattern) {
			return new Regex(pattern, RegexOptions.Compiled | RegexOptions.Multiline);
		}
	}

}
using System.Text.RegularExpressions;

namespace Fijo.Tasks.CorrectProjectFiles.Interface {
	public interface IFileContentReplaceService {
		void Replace(string file, Regex pattern, string replacement);
	}
}
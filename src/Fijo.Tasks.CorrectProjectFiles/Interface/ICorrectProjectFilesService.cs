namespace Fijo.Tasks.CorrectProjectFiles.Interface {
	public interface ICorrectProjectFilesService {
		void Correct(string dictionary, string filesWithExtention, string pattern, string replacement);
	}
}